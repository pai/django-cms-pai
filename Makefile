DOCKER=docker
DPATH=${CURDIR}

IMAGE_NAME=django-cms-pai
VERSION=$(shell python -c 'import backend;print(backend.__version__)')


export DPATH
.PHONY:	build-docker push-docker

build-docker:
	$(DOCKER) build . --network host -f ./compose/production/django/Dockerfile -t paiuolo/${IMAGE_NAME} -t paiuolo/${IMAGE_NAME}:${VERSION}

push-docker:
	$(DOCKER) push paiuolo/${IMAGE_NAME}:${VERSION}
	$(DOCKER) push paiuolo/${IMAGE_NAME}:latest
