from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.utils import timezone
from django.views import defaults as default_views
# from rest_framework.authtoken.views import obtain_auth_token
from django.views.decorators.http import last_modified
from django.contrib.flatpages.views import flatpage
from django.views.i18n import JavaScriptCatalog
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import gettext_lazy as _
# rpc
from rpc4django.views import serve_rpc_request

# django-cms-pai
from django.contrib.sitemaps.views import sitemap
from cms.sitemaps import CMSSitemap

# django-sso-app
from django_sso_app.urls import (urlpatterns as django_sso_app__urlpatterns,
                                 api_urlpatterns as django_sso_app__api_urlpatterns,
                                 i18n_urlpatterns as django_sso_app_i18n_urlpatterns)
# from django_sso_app.core.mixins import WebpackBuiltTemplateViewMixin

from backend.views import set_language_from_url
from backend.api.views import StatsView, schema_view


last_modified_date = timezone.now()
js_info_dict = {}

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': {'cmspages': CMSSitemap}}, name='sitemap'),
]
api_urlpatterns = []
_I18N_URLPATTERNS = []

urlpatterns += django_sso_app__urlpatterns
api_urlpatterns += django_sso_app__api_urlpatterns
_I18N_URLPATTERNS += django_sso_app_i18n_urlpatterns

urlpatterns += [
    # pai
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', last_modified(lambda req, **kw: last_modified_date)(JavaScriptCatalog.as_view()), js_info_dict,
        name='javascript-catalog'),

    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),

    # set language url
    url(r'^set_language/(?P<user_language>\w+)/$', set_language_from_url, name="set_language_from_url"),
]

#_I18N_URLPATTERNS += [
    # path('', WebpackBuiltTemplateViewMixin.as_view(template_name='pages/home.html'), name='home'),  # django-cms-pai
    # url(r'^search/(?P<path>.*)/$', WebpackBuiltTemplateViewMixin.as_view(template_name="search.html")),
#]

if settings.I18N_PATH_ENABLED:
    urlpatterns += i18n_patterns(
        *_I18N_URLPATTERNS
    )
else:
    urlpatterns += _I18N_URLPATTERNS



# Your stuff: custom urls includes go here
""" # django-cms-pai
urlpatterns += [
    path('users/', include('backend.users.urls', namespace='users')),

    # flatpages
    path('about/', flatpage, {'url': '/about/'}, name='about'),
]
"""

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# API URLS
""" pai
urlpatterns += [
    # API base url
    path("api/", include("config.api_router")),
    # DRF auth token
    path("auth-token/", obtain_auth_token),
]
"""

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            '400/',
            default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')},
        ),
        path(
            '403/',
            default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')},
        ),
        path(
            '404/',
            default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')},
        ),
        path('500/', default_views.server_error),
    ]

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]


api_urlpatterns += [
    url(r'^api/v1/_stats/$', StatsView.as_view(), name='stats'),

    url(r'^rpc/v2/$', serve_rpc_request),

    # your api here
]

# djangocms-blog
if settings.DJANGOCMS_BLOG_ENABLED:
    urlpatterns += [
        url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    ]

# extra
from .extra_urls import api_urlpatterns as extra_api_urlpatterns
if len(extra_api_urlpatterns):
    api_urlpatterns += extra_api_urlpatterns

urlpatterns += api_urlpatterns

urlpatterns += [
    url(r'^api/v1/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^api/v1/(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # url(r'^api/v1/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

# django-cms-pai

if 'djangocms_pai_contact' in settings.INSTALLED_APPS:
    from djangocms_pai_contact.urls import urlpatterns as djangocms_pai_contact_urls
    urlpatterns.append(url(r'^api/v1/contact/', include(djangocms_pai_contact_urls)))

if 'djangocms_pai_banners' in settings.INSTALLED_APPS:
    from djangocms_pai_banners.urls import urlpatterns as djangocms_pai_banners_urls
    urlpatterns.append(url(r'^api/v1/banners/', include(djangocms_pai_banners_urls)))

if 'djangocms_pai_newsletter' in settings.INSTALLED_APPS:
    from djangocms_pai_newsletter.urls import urlpatterns as djangocms_pai_newsletter_urls
    urlpatterns.append(url(r'^api/v1/newsletter/', include(djangocms_pai_newsletter_urls)))

if 'djangocms_pai_ghost_articles' in settings.INSTALLED_APPS:
    from djangocms_pai_ghost_articles.urls import urlpatterns as djangocms_pai_ghost_articles_urls
    urlpatterns.append(url(r'^api/v1/ghost/', include(djangocms_pai_ghost_articles_urls)))

"""
# flatpages

if settings.I18N_PATH_ENABLED:
    for lang, _name in settings.LANGUAGES:
        # flatpages
        urlpatterns.append(path(_('{}/about/'.format(lang)), flatpage, {'url': '/{}/about/'.format(lang)}, name='about-{}'.format(lang)))

urlpatterns += [
    path('<path:url>', include('django.contrib.flatpages.urls')),
]
"""

# extra

from .extra_urls import urlpatterns as extra_urlpatterns
from .extra_urls import i18n_urlpatterns as extra_i18n_urlpatterns

if len(extra_urlpatterns) > 0:
    urlpatterns += extra_urlpatterns

# django-cms-pai i18n
if settings.I18N_PATH_ENABLED:
    if len(extra_i18n_urlpatterns) > 0:
        urlpatterns += i18n_patterns(extra_i18n_urlpatterns)
    urlpatterns += i18n_patterns(url(r'^', include('cms.urls')))
else:
    if len(extra_i18n_urlpatterns) > 0:
        urlpatterns += extra_i18n_urlpatterns

    urlpatterns += [url(r'^', include('cms.urls'))]

# print('URLPATTERNS', urlpatterns)
