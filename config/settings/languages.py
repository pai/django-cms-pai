gettext = lambda s: s

LANGUAGES = (
    ## Customize this
    ('it', gettext('it')),
    ('en', gettext('en')),
    ('es', gettext('es')),
    ('pt', gettext('pt')),
    ('fr', gettext('fr')),
    ('de', gettext('de')),
)
