#!/usr/bin/env bash


### Restore database from a backup.
###
### Parameters:
###     <1> filename of an existing backup.
###
### Usage:
###     $ docker-compose -f <environment>.yml (exec |run --rm) postgres restore <1>

APP_NAME='django_cms'

set -o errexit
set -o pipefail
set -o nounset


working_dir="$(dirname ${0})"
source "${working_dir}/_sourced/constants.sh"
source "${working_dir}/_sourced/messages.sh"


if [[ -z ${1+x} ]]; then
    message_error "Backup filename is not specified yet it is a required parameter. Make sure you provide one and try again."
    exit 1
fi
backup_filename="${BACKUP_DIR_PATH}/${1}"
if [[ ! -f "${backup_filename}" ]]; then
    message_error "No backup with the specified filename found. Check out the 'backups' maintenance script output to see if there is one and try again."
    exit 1
fi

message_welcome "Restoring '${APP_NAME}' from the '${backup_filename}' backup..."

tar xvjf "${BACKUP_DIR_PATH}/${1}"


if [[ ${backup_filename} == *"public.gz" ]]
then
    echo "Public backup"
elif [[ ${backup_filename} == *"private.gz" ]]
then
    echo "Private backup"
else
    echo "loaddata backup"
    python manage.py loaddata dump_all.json
fi
