from django.db import models
from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool


class BundleExtension(PageExtension):
    css = models.TextField(null=True, blank=True)
    js = models.TextField(null=True, blank=True)
    ready_js = models.TextField(null=True, blank=True)

    css1 = models.FileField(upload_to='bundles', null=True, blank=True)

    bundle1 = models.FileField(upload_to='bundles', null=True, blank=True)
    bundle2 = models.FileField(upload_to='bundles', null=True, blank=True)
    bundle3 = models.FileField(upload_to='bundles', null=True, blank=True)
    bundle4 = models.FileField(upload_to='bundles', null=True, blank=True)
    bundle5 = models.FileField(upload_to='bundles', null=True, blank=True)

extension_pool.register(BundleExtension)
