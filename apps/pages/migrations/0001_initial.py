# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-26 13:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0020_old_tree_cleanup'),
    ]

    operations = [
        migrations.CreateModel(
            name='BundleExtension',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('css', models.TextField(blank=True, null=True)),
                ('js', models.TextField(blank=True, null=True)),
                ('ready_js', models.TextField(blank=True, null=True)),
                ('bundle1', models.FileField(blank=True, null=True, upload_to='bundles')),
                ('bundle2', models.FileField(blank=True, null=True, upload_to='bundles')),
                ('bundle3', models.FileField(blank=True, null=True, upload_to='bundles')),
                ('bundle4', models.FileField(blank=True, null=True, upload_to='bundles')),
                ('bundle5', models.FileField(blank=True, null=True, upload_to='bundles')),
                ('extended_object', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='cms.Page')),
                ('public_extension', models.OneToOneField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='draft_extension', to='pages.BundleExtension')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
