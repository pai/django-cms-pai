from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'apps.pages'

    def ready(self):
        try:
            from . import signals  # noqa F401
        except ImportError:
            pass
