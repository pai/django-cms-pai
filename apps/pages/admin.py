from django.contrib import admin
from cms.extensions import PageExtensionAdmin
from .models import BundleExtension


class BundleExtensionAdmin(PageExtensionAdmin):
    pass

admin.site.register(BundleExtension, BundleExtensionAdmin)
