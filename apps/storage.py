import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.utils.deconstruct import deconstructible


@deconstructible
class PrivateFileSystemStorage(FileSystemStorage):
    def __init__(self, subdir):
        self.subdir = subdir
        super(PrivateFileSystemStorage, self).__init__(location=os.path.join(settings.PRIVATE_ROOT, self.subdir))

    def __eq__(self, other):
        return self.subdir == other.subdir


# storage instance
custom_fs = PrivateFileSystemStorage(subdir='users')
