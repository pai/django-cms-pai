import os
import platform

from pytz import utc
from pytz import timezone as pytz_timezone

TZ = pytz_timezone('utc')


if platform.system() == 'Windows':
    def local_space_available(dir):
        """Return space available on local filesystem."""
        import ctypes
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(dir), None, None, ctypes.pointer(free_bytes))
        return free_bytes.value
else:
    def local_space_available(dir):
        destination_stats = os.statvfs(dir)
        return destination_stats.f_bsize * destination_stats.f_bavail


def run_task(f, logger, **kwargs):
    errors = []
    result = None

    try:
        result = f(**kwargs)

    except Exception as e:
        if hasattr(e, 'message'):
            msg = e.message
        else:
            msg = str(e)

        print('Task failed "{}"'.format(msg))
        logger.exception(msg)
        errors.append(msg)

    ret = {
        'data': {
            'result': result
        },
        'meta': {
            'status': 'OK' if len(errors) == 0 else 'ERROR',
            'errors': {}
        }
    }
    ret['meta']['errors'] = errors

    return ret


def utcisoformat(dt):
    """
    Return a datetime object in ISO 8601 format in UTC, without microseconds
    or time zone offset other than 'Z', e.g. '2011-06-28T00:00:00Z'. Useful
    for making Django DateTimeField values compatible with the
    jquery.localtime plugin.
    """
    # Convert datetime to UTC, remove microseconds, remove timezone, convert to string
    if dt is not None:
        if dt.tzinfo is None or dt.tzinfo.utcoffset(dt) is None:
            return TZ.localize(dt.replace(microsecond=0)).astimezone(utc).replace(tzinfo=None).isoformat() + 'Z'
        else:
            return dt.astimezone(utc).replace(microsecond=0).replace(tzinfo=None).isoformat() + 'Z'
